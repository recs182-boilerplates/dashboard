﻿# Host: localhost  (Version 5.7.14)
# Date: 2017-11-20 21:15:15
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "administrators"
#

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(64) DEFAULT NULL,
  `email` char(128) DEFAULT NULL,
  `password` char(128) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for table "pages"
#

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_feature` int(11) DEFAULT '0',
  `name` char(128) DEFAULT NULL,
  `page_ref` char(64) DEFAULT NULL,
  `sort` tinyint(3) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
