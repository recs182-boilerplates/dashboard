class cLogin{

	constructor($state, Post){
		this.state  = $state;
		this.Post = Post;
	}

	authLogin(data){
		const email = data.email;
		const password = data.password;

		this.Post('login/login', {email: email, password: password}).then(() => {
			location.reload();
		}, err => {
			//TODO: make visual error for EMAIL_NULL, PASSWORD_NULL and ADMINISTRATOR_INACTIVE
			console.log("err", err);
			swal({ title: 'Email ou senha errados.', text: 'Verifique os dados e tente novamente.', type: 'error', showConfirmButton: true });
		});
	}

}